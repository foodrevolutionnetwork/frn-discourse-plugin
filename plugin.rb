# frozen_string_literal: true

# name: frn-discourse-plugin
# about: Custom Changes for FRN Discourse Deploy
# version: 0.1
# authors: FRN Tech Team
# url: https://bitbucket.org/foodrevolutionnetwork/frn-discourse-plugin

after_initialize do
  class ::UserApiKeysController
    # THE FILE THIS MODIFIES: app/controllers/user_api_keys_controller.rb
    # 9 out of 10 software engineers recommend this
    # class -> module -> prepend pattern as the preferred way to monkey patch
    # The last engineer knows to avoid anti-patterns
    requires_login only: [:create_otp, :undo_revoke]
    skip_before_action :redirect_to_login_if_required, only: [:new, :create, :otp, :revoke, :revoke_all_api_keys_for_user]
    skip_before_action :verify_authenticity_token, only: [:create, :revoke, :revoke_all_api_keys_for_user]

    module FRNUserApiKeysPlugin
      def create
        # Because this endpoint is opened up
        # with the requires_login and skip_before_action above
        # Let's lock it back down with SSO
        parsed = Rack::Utils.parse_query(request.query_string)
        decoded = Base64.decode64(parsed["sso"])
        decoded_hash = Rack::Utils.parse_query(decoded)

        calculated = OpenSSL::HMAC.hexdigest("sha256", SiteSetting.discourse_connect_secret, parsed["sso"])

        if calculated != parsed["sig"]
          diags = "\n\nsso: #{parsed["sso"]}\n\nsig: #{parsed["sig"]}\n\nexpected sig: #{calculated}"
          if parsed["sso"] =~ /[^a-zA-Z0-9=\r\n\/+]/m
            raise ParseError, "The SSO field should be Base64 encoded, using only A-Z, a-z, 0-9, +, /, and = characters. Your input contains characters we don't understand as Base64, see http://en.wikipedia.org/wiki/Base64 #{diags}"
          else
            raise ParseError, "Bad signature for payload #{diags}"
          end
        end

        decoded_hash.each do |k, v|
          params[k.to_sym] = v
        end

        request.env[Auth::DefaultCurrentUserProvider::CURRENT_USER_KEY] = fetch_user_from_params
        super
      end

      def revoke_all_api_keys_for_user
        UserApiKey.where(client_id: params[:client_id]).destroy_all
      end
    end

    prepend FRNUserApiKeysPlugin
  end

  Discourse::Application.routes.append do
    # Adding revoke all keys endpoint above
    post "/user-api-key/revoke_all" => "user_api_keys#revoke_all_api_keys_for_user"
  end

  class ::TopicsController
    # THE FILE THIS MODIFIES: app/controllers/topics_controller.rb
    module FRNTopicsDigest
      # TODO: This function is duplicated below because I don't know how to figure out Rails namespaces
      def extend_basic_user_serializer(user, scope)
        basic_serializer = BasicUserSerializer.new(user, root: false)
        UserCardSerializer.new(user, scope: guardian).as_json[:user_card].slice(*(basic_serializer.attributes.keys + [:can_send_private_message_to_user]))
      end

      # New endpoint so we only have to call this to get first/last 5 posts in a topic
      # Instead of making a separate API call for each topic
      def topics_digest
        topic_ids = params[:topic_ids].split(",")
        if (!topic_ids.kind_of?(Array) || !topic_ids.length)
          raise Discourse::InvalidParameters.new("topic_ids must be provided in a comma-separated list as a query parameter")
        end
        request.env[Auth::DefaultCurrentUserProvider::CURRENT_USER_KEY] = current_user

        multiple_topics = Topic.where(id: topic_ids)

        digest_object = {}
        topic_ids.each do |t_id|
          digest_object[t_id] = {}
          this_topic = multiple_topics.filter{|t| t.id == t_id.to_i}[0]
          if !this_topic.nil?
            this_topic_view = TopicView.new(this_topic, current_user, {})
            digest_object[t_id]['original_poster'] = extend_basic_user_serializer(this_topic.user, guardian)
            all_posts = this_topic_view.filtered_posts.sort{|a,b| a.post_number - b.post_number}
            digest_object[t_id]['first'] = PostSerializer.new(all_posts[0], {scope:guardian, add_raw: true})
            digest_object[t_id]['recent'] = []
            digest_object[t_id]['recent'] = (all_posts.length > 3 ? all_posts[-3..] : (all_posts[1..] || [])).map{|p| PostSerializer.new(p,{scope:guardian, add_raw:true})}
          end
        end

        render json: digest_object
      end
    end

    prepend FRNTopicsDigest
  end

  Discourse::Application.routes.append do
    # Adding digest endpoint above
    get 'topics/digest' => "topics#topics_digest"
  end

  class ::TopicQuery
    # FILE THIS MODIFIES: lib/topic_query.rb
    module TopicQueryPlugin
      # Include muted topics in the digest
      def remove_muted(list, user, options)
        options[:include_muted] = true
        options[:state] = "muted"
        super(list, user, options)
      end

      # Paginate only 10 topics per page instead of the default 30
      def per_page_setting
        10
      end

    end

    prepend TopicQueryPlugin
  end

  class ::TopicView
    # FILE THIS MODIFIES: lib/topic_view.rb
    # Get last batch of posts instead of first batch
    module GetLastBatchOfTopicPosts
      def filter_posts(opts = {})
        if opts[:post_number] == "-1"
          # Return final batch of results
          opts[:post_number] = @topic.highest_post_number
        end
        super(opts)
      end
    end

    prepend GetLastBatchOfTopicPosts
  end


  class ::PostAlerter
    # FILE THIS MODIFIES: app/services/post_alerter.rb
    module NotificationsMoreUserInfo
      def create_notification(user, type, post, opts = {})
        # Goal here is to make some more basic information available to the caller
        opts[:custom_data] = {
          # post belongs to user and topic, so I can assume these values exist
          excerpt: post.excerpt,
          category_id: post.topic.category_id
        }

        if ([4,5].include?(type)) # 4=edit, 5=like
          # We have to grab this user from the DB
          notifying_user = User.find(opts[:user_id])
          opts[:custom_data][:notifying_user_name] = notifying_user.name
          opts[:custom_data][:avatar_template] = notifying_user.avatar_template
        else
          # Default, use the post in question
          opts[:custom_data][:notifying_user_name] = post.user.name
          opts[:custom_data][:avatar_template] = post.user.avatar_template
        end

        super(user,type,post,opts)
      end
    end

    prepend NotificationsMoreUserInfo
  end

  class ::SearchIndexer
    # FILE THIS MODIFIES: app/services/search_indexer.rb
    # Don't index username
    module DontIndexUsername
      def update_users_index(user_id, username, name, custom_fields)
        super(user_id, '', name, custom_fields)
      end
    end

    # singleton_class means append to class level, not merely instance level
    singleton_class.prepend DontIndexUsername
  end

  add_to_serializer(:found_user, :moderator) do
    # THE FILE THIS MODIFIES: app/serializers/found_user_serializer.rb
    object.moderator
  end

  add_to_serializer(:post, :user_badges) do
    # THE FILE THIS MODIFIES: app/serializers/post_serializer.rb
    object.user && object.user.badges.map{|b| BadgeSerializer.new(b, root: false)}
  end

  add_to_serializer(:notification, :category_id) do
    # THE FILE THIS MODIFIES: app/serializers/notification_serializer.rb
    object.topic ? object.topic.category_id : nil
  end

  add_to_serializer(:listable_topic, :notification_level) do
    # THE FILE THIS MODIFIES: app/serializers/listable_topic_serializer.rb
    # set a default if not present
    (object.user_data && object.user_data.notification_level) || NotificationLevels.topic_levels[:regular]
  end

  add_to_serializer(:topic_view, :first_post) do
    # THE FILE THIS MODIFIES: app/serializers/topic_view_serializer.rb
    PostSerializer.new(object.topic.first_post, scope: scope, root: false)
  end

  add_to_serializer(:topic_poster, :username) do
    # THE FILE THIS MODIFIES: app/serializers/topic_poster_serializer.rb
    object.user.username
  end

  add_to_serializer(:post, :can_send_private_message_to_user) do
    # THE FILE THIS MODIFIES: app/serializers/post_serializer.rb
    object.user && UserCardSerializer.new(object.user, scope: scope).can_send_private_message_to_user
  end

  add_to_serializer('TopicViewDetails', :original_poster) do
    # THE FILE THIS MODIFIES: app/serializers/topic_view_details_serializer.rb
    # Note the name change - rails 'classify' removes the plural
    # TODO: This function is duplicated above because I don't know how to figure out Rails namespaces
    basic_serializer = BasicUserSerializer.new(object.topic.user, root: false)
    UserCardSerializer.new(object.topic.user, scope: scope).as_json[:user_card].slice(*(basic_serializer.attributes.keys + [:can_send_private_message_to_user]))
  end

  add_to_serializer(:topic_list_item, :allowed_user_summary) do
    # THE FILE THIS MODIFIES: topic_list_serializer.rb
    # Get usernames of allowed users and add to serializer
    include_allowed_user_count? && object.allowed_users.map{|u| u.username }
  end

  add_to_serializer(:found_user, :can_send_private_message_to_user) do
    # THE FILE THIS MODIFIES: app/serializers/found_user_serializer.rb
    # Immediately let us know if the user will accept PMs
    UserCardSerializer.new(object, scope: scope).can_send_private_message_to_user
  end

  add_to_serializer(:poster, :can_send_private_message_to_user) do
    # THE FILE THIS MODIFIES: app/serializers/poster_serializer.rb
    # Add this attribute to poster serializer, too
    UserCardSerializer.new(User.find(object.id), scope: scope).can_send_private_message_to_user
  end

  add_to_serializer(:category_detailed, :reviewable_by_group_name) do
    # THE FILE THIS MODIFIES: app/serializers/category_detailed_serializer.rb
    # Add moderating_groups to categories returned in CategoryList
    SiteSetting.enable_category_group_moderation? && object.moderating_groups
  end

  class ::UserCardSerializer
    # THE FILE THIS MODIFIES: app/serializers/user_card_serializer.rb
    # User must also be in the right trust level to accept DMs
    module ExtendCanSendPrivateMessageToUser
      def can_send_private_message_to_user
        super && (scope.is_admin? || scope.is_moderator? || scope.is_staff?) ? true : object.in_any_groups?(SiteSetting.personal_message_enabled_groups_map) && object.user_option.allow_private_messages
      end
    end

    prepend ExtendCanSendPrivateMessageToUser
  end

  class ::UsersController
    # THE FILE THIS MODIFIES: app/controllers/users_controller.rb
    # Add scope to array serializer
    # Really don't like that I don't call super, instead am entirely replacing the function
    # But it's only a two line function and I have to do both here...
    module AddScope
      def serialize_found_users(users)
        each_serializer =
          SiteSetting.enable_user_status? ? FoundUserWithStatusSerializer : FoundUserSerializer

        { users: ActiveModel::ArraySerializer.new(users, each_serializer: each_serializer, scope: guardian).as_json }
      end
    end

    prepend AddScope
  end

  module ::UserNotificationsHelper
    # THE FILE THIS MODIFIES: app/helpers/user_notifications_helper.rb
    # Don't show the username in notification emails
    extend self
    def show_username_on_post(post)
      false
    end
  end

  class ::UserNotifications
    # THE FILE THIS MODIFIES: app/mailers/user_notifications.rb
    # Set show_tags_in_subject so we can have access to the product
    # when constructing the email notification url
    module AlwaysShowTags
      def notification_email(user,opts)
        opts[:show_tags_in_subject] = true
        super(user,opts)
      end
    end

    prepend AlwaysShowTags
  end

  class Email::MessageBuilder
    # THE FILE THIS MODIFIES: lib/email/message_builder.rb
    # Customizing the links in the email
    module FRNCustomizeLinks

      def get_product_name(opts)
        if opts[:show_category_in_subject]
          return opts[:show_category_in_subject].split(/\s|\//)[0]
        elsif opts[:show_tags_in_subject]
          return opts[:show_tags_in_subject].split(/\s|\//)[0]
        end
      end

      # Not currently in use, but leaving here for now as it may be useful later
      def is_content_item(opts)
        # this attribute for forum topics will look like
        #         "wlc Community/wlc Community XXXXXXX"
        # for content item comments will look like
        #         "wlc"
        get_product_name(opts) == opts[:show_category_in_subject]
      end

      def initialize(to, opts = nil)
        opts_custom_link = opts || {}

        if opts && !opts[:url].nil?
          product = ""

          # Can get product name from either of these
          if !opts[:show_category_in_subject].nil? || !opts[:show_tags_in_subject].nil?
            product = get_product_name(opts)
            opts_custom_link[:base_url] = "#{SiteSetting.discourse_connect_url}/products/#{product}/community"
          end

          # AND check essentially checks this is not a PM
          if !opts[:show_tags_in_subject].nil? && !opts[:show_category_in_subject].nil?
            content_item_slug = opts[:show_tags_in_subject].split(" ").filter{|t| t != product}[0]
            opts_custom_link[:base_url] = "#{SiteSetting.discourse_connect_url}/products/#{product}/#{content_item_slug}"
          end

          link_info = /^\/t\/([\w-]*)\/(\d+)\/(\d+)/.match(opts[:url])
          topic_id = link_info[2]
          post_id = opts[:post_id] # This I think I can just assume is there
          post_number = link_info[3]

          if opts[:private_reply]
            # This is a pm!
            opts_custom_link[:url] = "/#!/dm/" + topic_id.to_s
          else
            if post_number.to_s == "1"
              opts_custom_link[:url] = "/#!/topic/" + topic_id.to_s
            else
              opts_custom_link[:url] = "/#!/reply/" + post_id.to_s
            end
          end
        end

        # Fix the unsubscribe link
        if opts[:unsubscribe_url]
          unsubscribe_key = /#{Discourse.base_url}\/email\/unsubscribe\/(.*)/.match(opts[:unsubscribe_url])[1]
          opts_custom_link[:unsubscribe_url] = SiteSetting.discourse_connect_url + "/profile?openPreferencesModal=true&unsubscribeKey=#{unsubscribe_key}"
        end

        super(to,opts_custom_link)
      end

      def subject
        raw_subject = super
        if !@opts[:show_category_in_subject].nil?
          raw_subject.gsub!("[FRN","[#{get_product_name(@opts).upcase}")
        end
        raw_subject
      end
    end

    prepend FRNCustomizeLinks
  end

  module Email::BuildEmailHelper
    # THE FILE THIS MODIFIES: lib/email/build_email_helper.rb
    module BuildEmailPlugin
      # Replace @ mention usernames with names
      def build_email(user, opts)
        ["message","html_override"].each{|k|
          m = opts[k.to_sym]

          if m
            doc = Nokogiri::HTML5.fragment(m)

            doc.css(".mention, .mention-group").map{|d|
              n = Nokogiri::XML::Node.new("span",doc)
              n.content = "@#{d.attributes["data-username"].value}"
              n['style'] = "text-decoration: none; font-weight: bold; color: #{SiteSetting.email_link_color};"
              d.replace(n)
            }

            opts[k.to_sym] = doc.to_s
          end
        }

        super(user,opts)
      end

      # Don't link username to Discourse
      def build_email(user, opts)
        m = opts[:html_override]

        if m
          doc = Nokogiri::HTML5.fragment(m)

          doc.css(".username-link").map{|d|
            n = Nokogiri::XML::Node.new("span",doc)
            n.content = d.children.text
            n['style'] = "text-decoration: none; font-weight: bold; color: #{SiteSetting.email_link_color};"
            d.replace(n)
          }

          opts[:html_override] = doc.to_s
        end

        super(user,opts)
      end
    end

    prepend BuildEmailPlugin
  end

  class EmailControllerHelper::TopicEmailUnsubscriber
    # THE FILE THIS MODIFIES: lib/email_controller_helper/topic_email_unsubscriber.rb
    # Add option to untrack topic (bring down to normal)
    module UntrackTopic

      def prepare_unsubscribe_options(controller)
        super(controller)
        muted = TopicUser.notification_levels[:muted]
        topic = unsubscribe_key.associated_topic

        controller.instance_variable_set(
          :@topic_muted,
          TopicUser.exists?(user: key_owner, notification_level: muted, topic_id: topic.id)
        )
      end

      def unsubscribe(params)
        updated = super(params)
        topic = unsubscribe_key.associated_topic

        # Check if topic is nil happened in super, so this is safe
       if (params[:mute_topic] == true) || (params["mute_topic"] == true)
         TopicUser.where(topic_id: topic.id, user_id: key_owner.id)
           .update_all(notification_level: TopicUser.notification_levels[:muted])

         updated = true
       end

       if (params[:mute_topic] == false) || (params["mute_topic"] == false)
         TopicUser.where(topic_id: topic.id, user_id: key_owner.id)
           .update_all(notification_level: TopicUser.notification_levels[:regular])

         updated = true
       end

       updated
      end
    end

    prepend UntrackTopic
  end

  class ::EmailController
    # THE FILE THIS MODIFIES: app/controllers/email_controller.rb
    # Return a json
    module ReturnAsJSON
      def unsubscribe
        super

        if request.headers['Accept'] == 'application/json'
          # No real need to create a serializer here
          render json: {
            success:  'OK',
            unsubscribe_options: {
              found: @found,
              different_user: @different_user,
              type: @type,
              topic: @topic,
              user: BasicUserSerializer.new(@user, root: false).as_json,
              watching_topic: @watching_topic,
              topic_muted: @topic_muted,
              digest_unsubscribe: @digest_unsubscribe,
              digest_frequencies: @digest_frequencies,
              unsubscribed_from_all: @unsubscribed_from_all
            }
          }
        end
      end
    end

    prepend ReturnAsJSON
  end

  class ::EmailController
    # THE FILE THIS MODIFIES: app/controllers/email_controller.rb
    # This is monkeypatching a VERY central function in Rails
    # Don't do what Donny Don't does
    # This patching should ONLY happen within the EmailController,
    # and is ONLY relevant when we make a POST request to unsubscribe a user
    # This is here because we don't want to redirect users to a Discourse URL
    skip_before_action :verify_authenticity_token, only: [:perform_unsubscribe]
    module OverrideRedirectFunctions
      def redirect_to(url)
        if params[:override_redirect]
          return render json: {success: true}
        else
          super(url)
        end
      end

      def redirect_back(fallback_location: nil)
        if params[:override_redirect]
          return render json: {success: false}
        else
          super(fallback_location)
        end
      end
    end

    prepend OverrideRedirectFunctions
  end

  class ::SystemMessage
    # FILE THIS MODIFIES: lib/system_message.rb
    module SuppressSystemMessages
      def create(type, params = {})
        if !@recipient.admin && !@recipient.moderator
          # Set skip send email to true
          params[:post_alert_options] = {skip_send_email: true}
        end

        super(type, params)
      end
    end

    prepend SuppressSystemMessages
  end

  MessageBus.extra_response_headers_lookup do |env|
    # THE FILE THIS MODIFIES: config/initializers/004-message_bus.rb
    # cors_origin is used by config/initializers/008-rack-cors.rb

    possible_origins = GlobalSetting.cors_origin.split(",") + SiteSetting.cors_origins.split("|")
    (possible_origins.include?(env["HTTP_ORIGIN"]) ? [["Access-Control-Allow-Origin", env["HTTP_ORIGIN"]]] : [])
    # X-SILENCE-LOGGER is in the Allow-Headers of config/initializers/004-message_bus.rb
    # I have no idea why it must be added here as well
    .append(["Access-Control-Allow-Headers", "X-SILENCE-LOGGER, User-Api-Key, Discourse-Present, Dont-Chunk"])
  end
end
